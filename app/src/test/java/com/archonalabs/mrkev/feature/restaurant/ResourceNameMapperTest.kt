package com.archonalabs.mrkev.feature.restaurant

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Jakub Juroska on 3/7/20.
 */
class ResourceNameMapperTest {

    private val inputs = listOf("Vegalite", "Oáza", "Forky''s", "Tři Ocásci", "Zdravý život", "Veg8café", "Die Küche")
    private val valid = listOf("vegalite", "oaza", "forkys", "tri_ocasci", "zdravy_zivot", "veg8cafe", "die_kuche")

    @Test
    fun mapRestaurantNameToDrawableNameTest() {

        val outputs = inputs.map { ResourceNameMapper.mapRestaurantNameToDrawableName(it) }

        outputs.forEachIndexed { index, item ->
            assertEquals(valid[index], item)
        }
    }
}