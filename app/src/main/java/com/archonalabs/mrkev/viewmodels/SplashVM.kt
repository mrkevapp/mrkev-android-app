package com.archonalabs.mrkev.viewmodels

import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.archonalabs.mrkev.Constants
import com.archonalabs.mrkev.data.extension.getCurrentDateString
import com.archonalabs.mrkev.domain.feature.dailymenu.FetchDailyMenuUseCase
import com.archonalabs.mrkev.domain.feature.restaurant.FetchRestaurantsUseCase
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

/**
 * Created by Jakub Juroska on 2/15/20.
 */

const val RETRY_TIMEOUT = 5000L

class SplashVM(val fetchRestaurants : FetchRestaurantsUseCase, val fetchDailyMenu: FetchDailyMenuUseCase) : ViewModel() {

    var hasRestaurants = false
    var hasDailyMenu = false

    private val _isDone : MutableLiveData<Boolean> = MutableLiveData()
    val isDone : LiveData<Boolean> = _isDone

    fun initDataLoad(context: Context) {
        viewModelScope.launch {
            loadDailyMenu(context)
            loadRestaurants()
        }
    }

    private suspend fun loadRestaurants() {
        val restaurantsResult = fetchRestaurants()
        if (restaurantsResult.isSuccess()) {
            Timber.d("Restaurant isSuccess. Data %s", restaurantsResult.getOrNull())

            hasRestaurants = true

            _isDone.postValue(hasRestaurants && hasDailyMenu)
        } else if (restaurantsResult.isError()) {
            val error = restaurantsResult.errorOrNull()
            Timber.d(error?.throwable,"Restaurant isError. Data %s", error!!.message)
            delay(RETRY_TIMEOUT)
            loadRestaurants()
        }
    }

    private suspend fun loadDailyMenu(context: Context) {
        val dailyMenuResult = fetchDailyMenu(FetchDailyMenuUseCase.Params(Date().getCurrentDateString()))
        if (dailyMenuResult.isSuccess()) {
            Timber.d("DailyMenu isSuccess. Data %s", dailyMenuResult.getOrNull())

            hasDailyMenu = true

            _isDone.postValue(hasRestaurants && hasDailyMenu)
        } else if (dailyMenuResult.isError()) {
            val error = dailyMenuResult.errorOrNull()
            Timber.e(error?.throwable, "DailyMenu isError. Data %s", error!!.message)
            delay(RETRY_TIMEOUT)
            loadDailyMenu(context)
        }
    }
}