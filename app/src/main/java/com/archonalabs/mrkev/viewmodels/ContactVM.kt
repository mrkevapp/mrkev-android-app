package com.archonalabs.mrkev.viewmodels

import android.content.Context
import android.content.SharedPreferences
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import com.archonalabs.mrkev.Constants

/**
 * Created by Václav Rychtecký on 07/19/2021
 */
class ContactVM : ViewModel() {

    fun saveToPreferences(context : Context) {
        val sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFERENCES, FragmentActivity.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putBoolean(Constants.TUTORIAL_AGAIN_KEY, true)
        editor.apply()
    }

}