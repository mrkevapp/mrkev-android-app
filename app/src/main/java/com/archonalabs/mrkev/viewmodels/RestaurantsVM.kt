package com.archonalabs.mrkev.viewmodels

import android.content.Context
import android.content.SharedPreferences
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.archonalabs.mrkev.Constants
import com.archonalabs.mrkev.adapters.RestaurantPagerAdapter
import com.archonalabs.mrkev.data.extension.getCurrentDateString
import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.feature.dailymenu.FetchDailyMenuUseCase
import com.archonalabs.mrkev.domain.feature.restaurant.FetchRestaurantsUseCase
import com.archonalabs.mrkev.domain.model.Restaurant
import com.archonalabs.mrkev.feature.restaurant.ReloadRestaurantListener
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

/**
 * Created by Jakub Juroska on 10/28/19.
 */
class RestaurantsVM(val fetchRestaurants: FetchRestaurantsUseCase, val fetchDailyMenu: FetchDailyMenuUseCase) : ViewModel(), ReloadRestaurantListener {

    var selectedPosition = 0
    var errorCounter = 0

    //there could be Observable adapter instead of restaurants
    private val _restaurants : MutableLiveData<List<Restaurant>> = MutableLiveData()
    val restaurants : LiveData<List<Restaurant>> = _restaurants

    private val _isDone : MutableLiveData<Boolean> = MutableLiveData()
    val isDone : LiveData<Boolean> = _isDone

    init {
        reloadRestaurants()
    }

    fun getAdapter(childFragmentManager : FragmentManager) : RestaurantPagerAdapter {
        return RestaurantPagerAdapter(childFragmentManager, restaurants.value ?: emptyList(), this)
    }

    fun reloadMenu(context: Context){

        viewModelScope.launch {
            val dailyMenuResult = fetchDailyMenu(FetchDailyMenuUseCase.Params(Date().getCurrentDateString()))
            if (dailyMenuResult.isSuccess()) {
                _isDone.postValue(true)
            } else {
                if (errorCounter < 6){
                    _isDone.postValue(false)
                    val error = dailyMenuResult.errorOrNull()
                    Timber.e(error?.throwable, "DailyMenu isError. Data %s", error?.message)
                    delay(RETRY_TIMEOUT)
                    errorCounter++
                    reloadMenu(context)
                } else {
                    Toast.makeText(context, "Omlouváme se, server není dostupný. " +
                            "Zkontrolujte své připojení k Internetu", Toast.LENGTH_LONG).show()
                    errorCounter = 0
                    _isDone.postValue(true)
                }
            }
        }
    }

    override fun reloadRestaurants() {
        viewModelScope.launch {
            val restaurantsResult = fetchRestaurants()
            if (restaurantsResult is Result.Success) {
                _restaurants.postValue(restaurantsResult.data)
            } else  {
                Timber.w("Restaurants couldn't be loaded")
                _restaurants.postValue(emptyList())
            }
        }
    }

    fun saveToPreferences(sharedPreferences: SharedPreferences, boolean : Boolean, constant : String) {
        val editor = sharedPreferences.edit()
        editor.putBoolean(constant, boolean)
        editor.apply()
    }
}