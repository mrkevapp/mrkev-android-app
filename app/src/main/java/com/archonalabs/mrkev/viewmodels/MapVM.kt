package com.archonalabs.mrkev.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.feature.restaurant.FetchRestaurantsUseCase
import com.archonalabs.mrkev.domain.model.Restaurant
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * Created by Jakub Juroska on 2/16/20.
 */
class MapVM(private val fetchRestaurants : FetchRestaurantsUseCase) : ViewModel() {

    private val _restaurants : MutableLiveData<List<Restaurant>> = MutableLiveData()
    val restaurants : LiveData<List<Restaurant>> = _restaurants

    init {
        viewModelScope.launch {
            val restaurantsResult = fetchRestaurants()
            if (restaurantsResult is Result.Success) {
                _restaurants.postValue(restaurantsResult.data)
            } else  {
                Timber.w("Restaurants couldn't be loaded")
                _restaurants.postValue(emptyList())
            }
        }
    }
}