package com.archonalabs.mrkev.viewmodels

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.adapters.RestaurantRecyclerAdapter
import com.archonalabs.mrkev.data.extension.getCurrentDateString
import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.feature.dailymenu.FetchOneDailyMenuUseCase
import com.archonalabs.mrkev.domain.feature.restaurant.FetchSingleRestaurantUseCase
import com.archonalabs.mrkev.domain.feature.restaurant.MarkRestaurantAsFavoriteUseCase
import com.archonalabs.mrkev.domain.model.DailyMenu
import com.archonalabs.mrkev.domain.model.DailyMenuItem
import com.archonalabs.mrkev.domain.model.Restaurant
import com.archonalabs.mrkev.feature.dailymenu.DailyMenuItemMapper
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

/**
 * Created by Jakub Juroska on 2/16/20.
 */
class RestaurantDetailVM(private val fetchOneDailyMenu: FetchOneDailyMenuUseCase, private val markRestaurantAsFavorite : MarkRestaurantAsFavoriteUseCase, private val fetchSingleRestaurantUseCase : FetchSingleRestaurantUseCase) : ViewModel() {

    private val _dailyMenu : MutableLiveData<DailyMenu> = MutableLiveData()
    private val _restaurant : MutableLiveData<Restaurant> = MutableLiveData()
    val dailyMenu : LiveData<DailyMenu> = _dailyMenu
    val restaurant : LiveData<Restaurant> = _restaurant

    fun loadDailyMenu(restaurantId: Long) {
        viewModelScope.launch {
            val dailyMenuResult = fetchOneDailyMenu(params = FetchOneDailyMenuUseCase.Params(Date().getCurrentDateString(), restaurantId))
            if (dailyMenuResult is Result.Success) {
                _dailyMenu.postValue(dailyMenuResult.data)
            } else  {
                Timber.w("Daily menu couldn't be loaded")
                _dailyMenu.postValue(null)
            }
        }
    }

    fun loadOneRestaurant(restaurantId: Long){
        viewModelScope.launch {
            val oneRestaurant = fetchSingleRestaurantUseCase(params = FetchSingleRestaurantUseCase.Params(restaurantId))
            if (oneRestaurant is Result.Success){
                _restaurant.postValue(oneRestaurant.data)
            } else {
                Timber.w("One Restaurant couldn't be loaded")
            }
        }
    }

    fun mapIconClicked(restaurantId: Long) {
        viewModelScope.launch {
            markRestaurantAsFavorite(params = MarkRestaurantAsFavoriteUseCase.Params(restaurantId))
        }
    }

    fun prepareDataForAdapter(context: Context, soupList: List<DailyMenuItem>?, mainCourseList: List<DailyMenuItem>?, dessertList: List<DailyMenuItem>?) : RestaurantRecyclerAdapter{

        val dataForAdapter : MutableList<AdapterData> = mutableListOf()
        viewModelScope.launch {

            if (!soupList.isNullOrEmpty()) {
                dataForAdapter.add(AdapterData(context.getString(R.string.soupString), null, true))

                for (soup in soupList){
                    dataForAdapter.add(AdapterData(DailyMenuItemMapper.mapDailyMenuItemToFood(soup), soup.price, false))
                }
            }

            if (!mainCourseList.isNullOrEmpty()) {
                dataForAdapter.add(AdapterData(context.getString(R.string.mainCourseString), null, true))

                for (mainCourse in mainCourseList){
                    dataForAdapter.add(AdapterData(DailyMenuItemMapper.mapDailyMenuItemToFood(mainCourse), mainCourse.price, false))
                }
            }

            //For now we don't care
            /*if (!dessertList.isEmpty()){
                dataForAdapter.add(AdapterData(context.getString(R.string.dessertString), null, true))

                for (dessert in dessertList){
                    dataForAdapter.add(AdapterData(DailyMenuItemMapper.mapDailyMenuItemToFood(dessert), dessert.price, false))
                }
            }*/

            if (dataForAdapter.isNullOrEmpty()){
                dataForAdapter.add(AdapterData("empty", null, false))
            }
        }

        val textSizes = Pair(context.resources.getDimension(R.dimen.header_text_size), context.resources.getDimension(R.dimen.item_text_size))
        return RestaurantRecyclerAdapter(dataForAdapter, textSizes, context)
    }

    class AdapterData(val name: String, val price: String?, val isHeader : Boolean)
}