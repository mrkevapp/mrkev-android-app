package com.archonalabs.mrkev.feature.restaurant

/**
 * Created by Jakub Juroska on 3/7/20.
 */
interface ReloadRestaurantListener {
    fun reloadRestaurants()
}