package com.archonalabs.mrkev.feature.dailymenu

import android.text.TextUtils
import com.archonalabs.mrkev.domain.model.DailyMenuItem
import java.lang.StringBuilder

object DailyMenuItemMapper {
    fun mapDailyMenuItemToFood(dailyMenuItem: DailyMenuItem) : String {

        val stringBuilder = StringBuilder()

        stringBuilder.append(dailyMenuItem.name)

        if(!TextUtils.isEmpty(dailyMenuItem.allergens) && dailyMenuItem.allergens != "unknown" && dailyMenuItem.allergens != "[]"){
            stringBuilder.append(" (${dailyMenuItem.allergens})")
        }

        return stringBuilder.toString()
    }
}