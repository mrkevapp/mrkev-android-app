package com.archonalabs.mrkev.feature.restaurant

import java.text.Normalizer
import java.util.*


/**
 * Created by Jakub Juroska on 3/7/20.
 */
object ResourceNameMapper {
    fun mapRestaurantNameToDrawableName(name : String) : String {

        var result = name

        //remove '
        result = result.filter {
            !"'".contains(it)
        }

        //replace spaces
        result = result.replace(" ", "_")

        //remove accents
        result = stripAccents(result)

        //make all it lowercase
        result = result.toLowerCase(Locale.getDefault())

        return result
    }

    private fun stripAccents(input: String): String {
        val separated = Normalizer.normalize(input, Normalizer.Form.NFD)
        return separated.replace("[\\p{InCombiningDiacriticalMarks}]".toRegex(), "")
    }
}