package com.archonalabs.mrkev

/**
 * Created by Václav Rychtecký on 03/15/2021
 */
object Constants {

    //Shared preferences
    const val SHARED_PREFERENCES = "SHARED_PREFERENCES"
    const val TUTORIAL_KEY = "TUTORIAL_KEY"
    const val TUTORIAL_AGAIN_KEY = "TUTORIAL_AGAIN_KEY"
}