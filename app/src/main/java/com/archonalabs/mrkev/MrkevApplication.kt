package com.archonalabs.mrkev

import android.app.Application
import com.archonalabs.mrkev.data.di.mrkevModule
import com.archonalabs.mrkev.data.di.networkModule
import com.archonalabs.mrkev.data.feature.cache.CacheRecords
import com.archonalabs.mrkev.data.realm.MrkevRealmMigration
import com.archonalabs.mrkev.data.realm.MrkevRealmModule
import com.archonalabs.mrkev.di.viewModelModule
import io.realm.Realm
import io.realm.RealmConfiguration
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * Created by Jakub Juroska on 11/20/18.
 */
class MrkevApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        initKoin()

        Realm.init(this)

        val realmModule = getKoinInstance<MrkevRealmModule>()
        val realmConfig = RealmConfiguration.Builder()
                .name("mrkev.realm")
                .modules(realmModule)
                .migration(getKoinInstance<MrkevRealmMigration>())
                .schemaVersion(1)
                .build()

        Realm.setDefaultConfiguration(realmConfig)

        //fixme: temp fix, init should be in Cache Records getter
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            val cacheRecordsMissing = realm.where(CacheRecords::class.java).findFirst() == null
            if (cacheRecordsMissing) {
                realm.executeTransaction {
                    it.copyToRealmOrUpdate(CacheRecords(id = 0))
                }
            }
        }
    }

    private fun initKoin() {

        startKoin {
            androidContext(this@MrkevApplication)
            modules(
                mrkevModule + networkModule + viewModelModule
            )
        }
    }

    inline fun <reified T : Any> getKoinInstance(): T {
        return object : KoinComponent {
            val value: T by inject()
        }.value
    }
}
