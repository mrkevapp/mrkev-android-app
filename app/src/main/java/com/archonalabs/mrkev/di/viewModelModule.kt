package com.archonalabs.mrkev.di

import com.archonalabs.mrkev.viewmodels.*
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

/**
 * Created by Jakub Juroska on 2/15/20.
 */
val viewModelModule : Module = module {

    viewModel { SplashVM(get(), get()) }

    viewModel { RestaurantsVM(get(), get()) }

    viewModel { MapVM(get()) }

    viewModel { RestaurantDetailVM(get(), get(), get()) }

    viewModel { ContactVM() }
}