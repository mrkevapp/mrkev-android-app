package com.archonalabs.mrkev.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.archonalabs.mrkev.domain.model.Restaurant
import com.archonalabs.mrkev.feature.restaurant.ReloadRestaurantListener
import com.archonalabs.mrkev.fragments.RestaurantDetailFragment
import timber.log.Timber

/**
 * Created by Václav Rychtecký on 07/20/2019
 */
class RestaurantPagerAdapter(
        manager : FragmentManager,
        private val restaurants : List<Restaurant>,
        private val reloadRestaurantListener: ReloadRestaurantListener) : FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {

        Timber.d("Requesting item with position %d", position)
        val restaurant = restaurants[position]

        return RestaurantDetailFragment.newInstance(restaurant.id, reloadRestaurantListener)
    }

    override fun getCount(): Int {
        return restaurants.size
    }
}