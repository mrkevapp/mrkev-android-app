package com.archonalabs.mrkev.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.viewmodels.RestaurantDetailVM
import kotlinx.android.synthetic.main.restaurant_recycler_item.view.*

class RestaurantRecyclerAdapter(private val list : List<RestaurantDetailVM.AdapterData>, private val textSizes : Pair<Float, Float>, private val context : Context) : RecyclerView.Adapter<RestaurantRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.restaurant_recycler_item, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.makeItem(list[position], textSizes, context)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun makeItem(item: RestaurantDetailVM.AdapterData, textSizes: Pair<Float, Float>, context : Context) {

            if (item.name == "empty"){
                itemView.restaurant_recycler_item_food_empty.visibility = View.VISIBLE
                itemView.restaurant_recycler_item_food_empty_text.visibility = View.VISIBLE
                itemView.restaurant_recycler_item_food_empty_text.text = context.resources.getString(R.string.no_data)
            } else {
                itemView.restaurant_recycler_item_food.text = item.name
                if(item.isHeader){
                    itemView.restaurant_recycler_item_food.typeface = ResourcesCompat.getFont(context, R.font.montserrat_bold)
                    itemView.restaurant_recycler_item_price.visibility = View.GONE
                    itemView.restaurant_recycler_item_food.textSize = textSizes.first
                } else {
                    itemView.restaurant_recycler_item_food.textSize = textSizes.second
                    if (item.price != null){
                        itemView.restaurant_recycler_item_price.text = item.price
                        itemView.restaurant_recycler_item_price.textSize = textSizes.second
                        itemView.restaurant_recycler_item_price.visibility = View.VISIBLE
                    }
                }
            }
        }
    }
}
