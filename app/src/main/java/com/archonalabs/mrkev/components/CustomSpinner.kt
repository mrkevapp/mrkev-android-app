package com.archonalabs.mrkev.components

import android.content.Context
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import android.view.animation.AnimationUtils
import androidx.constraintlayout.widget.ConstraintLayout
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.interfaces.SpinnerInterface
import kotlinx.android.synthetic.main.custom_spinner_layout.view.*

/**
 * Created by Václav Rychtecký on 07/19/2021
 */
class CustomSpinner @JvmOverloads constructor(context: Context,
                                              attrs: AttributeSet? = null,
                                              defStyleAttr: Int = 0)
    : ConstraintLayout(context, attrs, defStyleAttr) {

    //In future maybe refactor to recycler? For future projects?

    private val view : View = View.inflate(context, R.layout.custom_spinner_layout, this)
    private lateinit var spinnerInterface : SpinnerInterface

    init {
        onSelect()
        view.custom_spinner_button_one.text = resources.getStringArray(R.array.options)[0].toString()
        view.custom_spinner_button_two.text = resources.getStringArray(R.array.options)[1].toString()
    }

    fun rollSpinner(){
        view.visibility = View.VISIBLE
        startAnimation()
    }

    private fun onSelect(){
        view.custom_spinner_button_one.setOnClickListener {
            hideSpinner()
            spinnerInterface.tutorial()
            spinnerInterface.sendBoolean(false)
        }
        view.custom_spinner_button_two.setOnClickListener {
            spinnerInterface.openInfoPopUp()
            hideSpinner()
            spinnerInterface.sendBoolean(false)
        }
    }

    fun startAnimation(){
        val animation = AnimationUtils.loadAnimation(context, R.anim.top_to_bottom)
        view.custom_spinner_animation_constraint.startAnimation(animation)
    }

    fun hideSpinner() {
        val animation = AnimationUtils.loadAnimation(context, R.anim.bottom_to_top)
        view.custom_spinner_animation_constraint.startAnimation(animation)
        Handler().postDelayed(Runnable {
            view.visibility = View.GONE
        }, 250)
    }

    fun setSpinnerInterface(spinnerInterface: SpinnerInterface){
        this.spinnerInterface = spinnerInterface
    }
}