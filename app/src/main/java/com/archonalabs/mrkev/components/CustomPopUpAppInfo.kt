package com.archonalabs.mrkev.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.archonalabs.mrkev.R

/**
 * Created by Václav Rychtecký on 07/19/2021
 */
class CustomPopUpAppInfo @JvmOverloads constructor(context: Context,
                                                   attrs: AttributeSet? = null,
                                                   defStyleAttr: Int = 0)
    : ConstraintLayout(context, attrs, defStyleAttr) {

    //Not practical for future? What if we need more popups?

    private val view : View = View.inflate(context, R.layout.app_info_pop_up_layout, this)
    private val versionText : TextView = view.findViewById(R.id.info_version)

    fun setText(version: String) {
        versionText.text = String.format(resources.getString(R.string.version), version)
    }
}