package com.archonalabs.mrkev.activity

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.fragments.ContactFragment
import com.archonalabs.mrkev.fragments.MapFragment
import com.archonalabs.mrkev.fragments.RestaurantFragment
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber


const val RESTAURANT_TAG = "restaurant"
const val MAP_TAG = "map"
const val CONTACT_TAG = "contact"
const val ERROR_DIALOG_REQUEST = 9001
const val LOCATION_PERMISSION_REQUEST_CODE = 1234

class MainActivity : FragmentActivity() {

    private val requiredPermissions = listOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    private var lateLocationPermission = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.itemIconTintList = null

        if (savedInstanceState != null) {
            //todo po přesunutí na pozadí pomocí backbutton by se mělo nastavit to co se uložilo
            Timber.d("SavedInstance isn't null")

        } else {
            showFragment(RestaurantFragment.newInstance(), RESTAURANT_TAG)
            navigation.menu.findItem(R.id.dailymenu_navigation).setIcon(R.drawable.daily_selected)
        }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    fun showFragment(fragment : Fragment, tag : String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, tag)
        transaction.commit()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        val map = navigation.menu.findItem(R.id.map_navigation)
        val dailymenu = navigation.menu.findItem(R.id.dailymenu_navigation)
        val contact = navigation.menu.findItem(R.id.contact_navigation)

        when (item.itemId) {
            R.id.dailymenu_navigation -> {

                map.icon = getDrawable(R.drawable.map_not_selected)
                dailymenu.icon = getDrawable(R.drawable.daily_selected)
                contact.icon = getDrawable(R.drawable.contact_not_selected)

                Timber.d("Daily menu fragment")
                showFragment(RestaurantFragment.newInstance(), RESTAURANT_TAG)
                return@OnNavigationItemSelectedListener true
            }
            R.id.map_navigation -> {
                Timber.d("Map fragment")

                map.icon = getDrawable(R.drawable.map_selected)
                dailymenu.icon = getDrawable(R.drawable.daily_not_selected)
                contact.icon = getDrawable(R.drawable.contact_not_selected)

                if (checkLocationPermissions()) {
                    showFragment(MapFragment.newInstance(), MAP_TAG)
                } else {
                    if (isServicesOK()) {
                        if (canShowPermissionRationaleDialog()) {
                            showMessageOKCancel(this@MainActivity, R.string.permission_message, DialogInterface.OnClickListener { dialog, which ->
                                lateLocationPermission = true
                                getLocationPermission()
                                dialog.dismiss()
                            })
                        } else {
                            lateLocationPermission = true
                            getLocationPermission()
                        }
                    }
                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.contact_navigation -> {
                Timber.d("Contact fragment")

                map.icon = getDrawable(R.drawable.map_not_selected)
                dailymenu.icon = getDrawable(R.drawable.daily_not_selected)
                contact.icon = getDrawable(R.drawable.contact_selected)

                showFragment(ContactFragment.newInstance(), CONTACT_TAG)
                return@OnNavigationItemSelectedListener true

            }
        }
        false
    }

    fun checkLocationPermissions() : Boolean {
        for (permission in requiredPermissions) {
            if (ActivityCompat.checkSelfPermission(this@MainActivity, permission) == PackageManager.PERMISSION_GRANTED) {
                return true
            }
        }

        return false
    }

    private fun getLocationPermission() {
        if (!checkLocationPermissions()) {
            val permission = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
            ActivityCompat.requestPermissions(this, permission, LOCATION_PERMISSION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    if (lateLocationPermission){
                        showFragment(MapFragment.newInstance(), MAP_TAG)
                    }
                } else {
                    Timber.w("Permission not GRANTED")
                }
            }
        }
    }

    private fun isServicesOK(): Boolean {

        val available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(applicationContext)

        when {
            available == ConnectionResult.SUCCESS -> {
                return true
            }
            GoogleApiAvailability.getInstance().isUserResolvableError(available) -> {
                val dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, available, ERROR_DIALOG_REQUEST)
                dialog.show()
            }
            else -> {
                Timber.d("We can't make map request")
            }
        }

        return false
    }

    private fun canShowPermissionRationaleDialog(): Boolean {
        var shouldShowRationale = false
        for (permission in getUnGrantedPermissionsList(requiredPermissions)) {
            val shouldShow = ActivityCompat.shouldShowRequestPermissionRationale(this@MainActivity, permission)
            if (shouldShow) {
                shouldShowRationale = true
            }
        }
        return shouldShowRationale
    }

    private fun getUnGrantedPermissionsList(requiredPermissions : List<String>): List<String> {
        val list = mutableListOf<String>()
        for (permission in requiredPermissions) {
            val result = ActivityCompat.checkSelfPermission(this@MainActivity, permission)
            if (result != PackageManager.PERMISSION_GRANTED) {
                list.add(permission)
            }
        }
        return list
    }

    private fun showMessageOKCancel(activity : Activity, messageStringId: Int, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(activity)
                .setMessage(messageStringId)
                .setPositiveButton(android.R.string.ok, okListener)
                .setNegativeButton(android.R.string.cancel, DialogInterface.OnClickListener { dialogInterface, i ->
                    Timber.i("Permission denied, do nothing?")
                })
                .create()
                .show()
    }
}


