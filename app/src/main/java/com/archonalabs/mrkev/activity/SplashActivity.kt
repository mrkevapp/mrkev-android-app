package com.archonalabs.mrkev.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.viewmodels.SplashVM
import org.koin.androidx.viewmodel.ext.android.viewModel

const val SPLASH_TIME_OUT = 25500L

class SplashActivity : AppCompatActivity() {

    private val viewModel : SplashVM by viewModel()
    private val handler : Handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_splash)

        handler.postDelayed({

            Toast.makeText(applicationContext, "Omlouváme se, server není dostupný. " +
                    "Zkontrolujte své připojení k Internetu", Toast.LENGTH_LONG).show()

            startNextActivity()

        }, SPLASH_TIME_OUT)

        viewModel.initDataLoad(applicationContext)

        viewModel.isDone.observe(this, androidx.lifecycle.Observer { shouldContinue ->
            if (shouldContinue) {
                startNextActivity()
            }
        })
    }

    private fun startNextActivity() {
        handler.removeCallbacksAndMessages(null)
        val mainActivity = Intent(this@SplashActivity, MainActivity::class.java)
        startActivity(mainActivity)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        finish()
    }
}