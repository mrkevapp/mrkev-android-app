package com.archonalabs.mrkev.utils

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.google.android.gms.maps.model.LatLng

/**
 * Created by Jakub Juroska on 3/7/20.
 */
object NavigationUtils {

    fun navigateTo(activity : Activity, location: LatLng) {
        val uri: Uri = Uri.parse("https://www.google.com/maps/search/?api=1&query=${location.latitude},${location.longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, uri)
        mapIntent.setPackage("com.google.android.apps.maps")
        activity.startActivity(mapIntent)
    }

    fun navigateTo(activity : Activity, latitude : String, longitude : String) {
        val uri: Uri = Uri.parse("https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}")
        val mapIntent = Intent(Intent.ACTION_VIEW, uri)
        mapIntent.setPackage("com.google.android.apps.maps")
        activity.startActivity(mapIntent)
    }
}