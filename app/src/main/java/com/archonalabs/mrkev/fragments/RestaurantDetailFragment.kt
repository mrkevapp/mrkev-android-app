package com.archonalabs.mrkev.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.feature.restaurant.ReloadRestaurantListener
import com.archonalabs.mrkev.feature.restaurant.ResourceNameMapper
import com.archonalabs.mrkev.utils.NavigationUtils
import com.archonalabs.mrkev.viewmodels.RestaurantDetailVM
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.restaurant_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * Created by Václav Rychtecký on 07/20/2019
 */
const val ID = "id"

class RestaurantDetailFragment : Fragment() {

    private lateinit var reloadListener: ReloadRestaurantListener
    private var restaurantID : Long = 0

    private val viewModel : RestaurantDetailVM by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.restaurant_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null) {
            restaurantID = arguments?.getLong(ID) ?: 0

            viewModel.loadDailyMenu(restaurantID)
            viewModel.loadOneRestaurant(restaurantID)
        }

        viewModel.dailyMenu.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it != null) {
                daily_menu_recycler.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = viewModel.prepareDataForAdapter(requireContext(), it.soup, it.mainCourse, it.dessert)
                }
            } else {
                Timber.d("Restaurace nemá žádné menu")
                daily_menu_recycler.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = viewModel.prepareDataForAdapter(requireContext(), null, null, null)
                }
            }
        })

        viewModel.restaurant.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it != null){
                val id = resources.getIdentifier(ResourceNameMapper.mapRestaurantNameToDrawableName(it.name), "drawable", requireContext().packageName)
                Picasso.get().load(id).fit().centerCrop().into(daily_menu_header_image)
                setFavoriteImage(it.favorite)
            } else {
                Timber.d("Nemáme žádnou restauraci *question mark*")
            }
        })

        daily_menu_favorite_button.setOnClickListener {
            viewModel.mapIconClicked(restaurantID)

            reloadListener.reloadRestaurants()
        }

        restaurant_detail_navigation_button.setOnClickListener{
            val restaurant = viewModel.restaurant.value
            restaurant?.let{
                NavigationUtils.navigateTo(requireActivity(), it.latitude, it.longitude)
            } ?: Timber.w("Restaurant missing")
        }
    }

    private fun setFavoriteImage(isFavorite : Boolean){
        if (isFavorite) {
            daily_menu_favorite_button.setImageDrawable(context?.getDrawable(R.drawable.mrkev_favorite_on))
        } else {
            daily_menu_favorite_button.setImageDrawable(context?.getDrawable(R.drawable.mrkev_favorite_off))
        }
    }

    private fun setReloadListener(reloadRestaurantListener: ReloadRestaurantListener) {
        this.reloadListener = reloadRestaurantListener
    }

    companion object {

        fun newInstance(id : Long, reloadRestaurantListener: ReloadRestaurantListener) : RestaurantDetailFragment {

            val args = Bundle()
            args.putLong(ID, id)
            val fragment = RestaurantDetailFragment()
            fragment.setReloadListener(reloadRestaurantListener)
            fragment.arguments = args
            return fragment
        }
    }
}