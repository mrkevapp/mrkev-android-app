package com.archonalabs.mrkev.fragments

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.archonalabs.mrkev.BuildConfig
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.activity.MainActivity
import com.archonalabs.mrkev.activity.RESTAURANT_TAG
import com.archonalabs.mrkev.interfaces.SpinnerInterface
import com.archonalabs.mrkev.viewmodels.ContactVM
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_contact.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ContactFragment : Fragment(), SpinnerInterface {

    private var isSpinnerRoll = false

    private val viewModel : ContactVM by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contact_spinner.setSpinnerInterface(this)

        contact_app_info_pop_up.setText(BuildConfig.VERSION_NAME)

        contact_app_info_pop_up.setOnClickListener {
            contact_app_info_pop_up.visibility = View.GONE
        }

        contact_email_button.setOnClickListener {

            if (isSpinnerRoll){
                hideSpinner()
            }

            contact_app_info_pop_up.visibility = View.GONE

            val mIntent = Intent(Intent.ACTION_SEND)
            mIntent.data = Uri.parse("mailto:")
            mIntent.type = "text/plain"
            mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(context!!.getString(R.string.archona)))

            try {
                startActivity(Intent.createChooser(mIntent, requireContext().getString(R.string.email_selector_title)))
            } catch (e : Exception){
                Timber.e("Email exception")
            }
        }

        contact_layout.setOnClickListener {
            if (isSpinnerRoll){
                hideSpinner()
            }
            contact_app_info_pop_up.visibility = View.GONE
        }

        contact_options.setOnClickListener {
            if (isSpinnerRoll){
                hideSpinner()
            } else {
                isSpinnerRoll = true
                contact_spinner.rollSpinner()
            }
            contact_app_info_pop_up.visibility = View.GONE
        }
    }

    private fun hideSpinner(){
        isSpinnerRoll = false
        contact_spinner.hideSpinner()
    }

    companion object{
        fun newInstance() : ContactFragment {
            return ContactFragment()
        }
    }

    override fun sendBoolean(boolean: Boolean) {
        isSpinnerRoll = boolean
    }

    override fun openInfoPopUp() {
        contact_app_info_pop_up.visibility = View.VISIBLE
    }

    override fun tutorial() {
        val builder = AlertDialog.Builder(context)
        builder.setTitle("Tutoriál")
        builder.setTitle("Přejete si projít tutoriál k aplikaci?")
        builder.setPositiveButton("ANO") { dialogInterface, _ ->
            dialogInterface.dismiss()
            viewModel.saveToPreferences(context!!)
            (activity as MainActivity).navigation.menu.getItem(2).setIcon(R.drawable.contact_not_selected)
            (activity as MainActivity).showFragment(RestaurantFragment.newInstance(), RESTAURANT_TAG)
            (activity as MainActivity).navigation.menu.findItem(R.id.dailymenu_navigation).setIcon(R.drawable.daily_selected)
        }
        builder.setNegativeButton("NE") { dialogInterface, _ ->
            dialogInterface.dismiss()
        }
        builder.show()
    }
}
