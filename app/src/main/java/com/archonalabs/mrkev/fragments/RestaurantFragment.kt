package com.archonalabs.mrkev.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.archonalabs.mrkev.Constants
import com.archonalabs.mrkev.R
import com.archonalabs.mrkev.activity.SPLASH_TIME_OUT
import com.archonalabs.mrkev.viewmodels.RestaurantsVM
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_restaurant.*
import kotlinx.android.synthetic.main.restaurant_detail.*
import kotlinx.android.synthetic.main.swipe_tutorial_layout.view.*
import me.relex.circleindicator.CircleIndicator
import me.toptas.fancyshowcase.FancyShowCaseQueue
import me.toptas.fancyshowcase.FancyShowCaseView
import me.toptas.fancyshowcase.FocusShape
import me.toptas.fancyshowcase.listener.OnCompleteListener
import me.toptas.fancyshowcase.listener.OnViewInflateListener
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


class RestaurantFragment : Fragment() {

    private lateinit var indicator: CircleIndicator
    private lateinit var mPager: ViewPager
    private val viewModel : RestaurantsVM by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_restaurant, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        indicator = view.findViewById(R.id.indicator)

        viewModel.restaurants.observe(viewLifecycleOwner, Observer {
            refreshAdapter(viewModel.selectedPosition)
        })

        viewModel.isDone.observe(viewLifecycleOwner, Observer {
            if (it){
                endLoading()
                refreshAdapter(viewModel.selectedPosition)
            } else {
                startLoading()
            }
        })

        startTutorial()
    }

    private fun startTutorial() {

        val sharedPreferences = requireContext().getSharedPreferences(Constants.SHARED_PREFERENCES, FragmentActivity.MODE_PRIVATE)
        val tutorialKey = sharedPreferences.getBoolean(Constants.TUTORIAL_KEY, false)
        val tutorialAgain = sharedPreferences.getBoolean(Constants.TUTORIAL_AGAIN_KEY, false)

        if (tutorialAgain){
            //Handler needed, otherwise is code to fast, the components are null.. :(
            Handler().postDelayed({
                tutorial(sharedPreferences)
            },100)
        }

        if (!tutorialKey){

            val builder = AlertDialog.Builder(context)
            builder.setTitle("Tutoriál")
            builder.setTitle("Přejete si projít tutoriál k aplikaci?")
            builder.setPositiveButton("ANO", DialogInterface.OnClickListener { dialogInterface, _ ->
                dialogInterface.dismiss()
                tutorial(sharedPreferences)
            })
            builder.setNegativeButton("NE", DialogInterface.OnClickListener { dialogInterface, _ ->
                viewModel.saveToPreferences(sharedPreferences, true, Constants.TUTORIAL_KEY)
                viewModel.saveToPreferences(sharedPreferences, false, Constants.TUTORIAL_AGAIN_KEY)
                val insideBuilder = AlertDialog.Builder(context)
                insideBuilder.setMessage("Tutoriál lze kdykoliv znovu spustit. Tuto možnost naleznete na poslední obrazovce aplikace. Dobrou chuť!")
                insideBuilder.setPositiveButton(resources.getString(R.string.tutorial_dissmiss)) { di, _ ->
                    di.dismiss()
                }
                insideBuilder.show()
                dialogInterface.dismiss()
            })
            builder.show()
        }
    }

    private fun refreshAdapter(currentPosition: Int) {

        //TODO binding

        mPager = requireView().findViewById(R.id.view_pager)
        mPager.adapter = viewModel.getAdapter(childFragmentManager)
        mPager.currentItem = currentPosition

        indicator.animatePageSelected(currentPosition)
        indicator.setViewPager(mPager)
    }


    private fun tutorial(sharedPreferences: SharedPreferences) {

        //Navigation & Logo tutorial
        val fancyShowCaseView1 = FancyShowCaseView.Builder(requireActivity())
            .title(getString(R.string.tutorial_logo))
            .titleStyle(R.style.textStyle, Gravity.CENTER)
            .fitSystemWindows(true)
            .focusOn(restaurant_detail_navigation_button)
            .focusCircleRadiusFactor(0.8)
            .build()

        //Favorite button tutorial
        val fancyShowCaseView2 = FancyShowCaseView.Builder(requireActivity())
            .title(getString(R.string.tutorial_favorite))
            .titleStyle(R.style.textStyle, Gravity.CENTER)
            .fitSystemWindows(true)
            .focusOn(daily_menu_favorite_button)
            .build()

        //Swipe tutorial
        //TODO CUSTOM LAYOUT WITH ANIMATIONS
        val fancyShowCaseView3 = FancyShowCaseView.Builder(requireActivity())
            .fitSystemWindows(true)
            .customView(R.layout.swipe_tutorial_layout, object : OnViewInflateListener{
                override fun onViewInflated(view: View) {

                    val animation = AnimationUtils.loadAnimation(context, R.anim.left_to_right)
                    val secAnimation = AnimationUtils.loadAnimation(context, R.anim.right_to_left)

                    startAnimation(animation, secAnimation, view)
                }
            })
            .build()

        val fancyShowCaseView4 = FancyShowCaseView.Builder(requireActivity())
            .title(getString(R.string.tutorial_bottom))
            .titleStyle(R.style.textStyle, Gravity.CENTER)
            .fitSystemWindows(true)
            .focusOn(requireActivity().navigation)
            .focusShape(FocusShape.ROUNDED_RECTANGLE)
            .build()

        val queue = FancyShowCaseQueue()
            .add(fancyShowCaseView1)
            .add(fancyShowCaseView2)
            .add(fancyShowCaseView3)
            .add(fancyShowCaseView4)


        queue.completeListener = object : OnCompleteListener{
            override fun onComplete() {
                viewModel.saveToPreferences(sharedPreferences, true, Constants.TUTORIAL_KEY)
                viewModel.saveToPreferences(sharedPreferences, false, Constants.TUTORIAL_AGAIN_KEY)
            }
        }

        queue.show()
    }

    private fun startAnimation(animation: Animation?, secAnimation: Animation?, view: View) {
        view.tutorial_swipe.startAnimation(animation)
        val handler = Handler()
        handler.postDelayed(Runnable {
            view.tutorial_swipe.startAnimation(secAnimation)
        }, 1400)

        handler.postDelayed(Runnable {
            startAnimation(animation, secAnimation, view)
        }, 2800)
    }

    override fun onPause() {
        super.onPause()
        Timber.d("OnPause")
        viewModel.selectedPosition = mPager.currentItem
    }

    override fun onResume() {
        super.onResume()

        Timber.d("OnResume last position: %d", viewModel.selectedPosition)
        viewModel.reloadMenu(requireContext())
    }

    private fun startLoading(){
        restaurant_fragment_progress.visibility = View.VISIBLE
        restaurant_fragment_loading_text.visibility = View.VISIBLE
        view_pager.visibility = View.GONE
        indicator.visibility = View.GONE
    }

    private fun endLoading(){
        restaurant_fragment_progress.visibility = View.GONE
        restaurant_fragment_loading_text.visibility = View.GONE
        view_pager.visibility = View.VISIBLE
        indicator.visibility = View.VISIBLE
    }

    companion object{
        fun newInstance() : RestaurantFragment {
            return RestaurantFragment()
        }
    }
}