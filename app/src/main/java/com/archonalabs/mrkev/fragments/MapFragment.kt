package com.archonalabs.mrkev.fragments

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.archonalabs.mrkev.domain.model.Restaurant
import com.archonalabs.mrkev.utils.NavigationUtils
import com.archonalabs.mrkev.viewmodels.MapVM
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ui.IconGenerator
import kotlinx.android.synthetic.main.fragment_map.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber


/**
 * Created by Václav Rychtecký on 07/20/2019
 */
class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private var mContext: Context? = null
    private val viewModel : MapVM by viewModel()

    private var mMap: GoogleMap? = null

    private lateinit var iconGenerator: IconGenerator
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var customerMarker: Marker? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.archonalabs.mrkev.R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.restaurants.observe(viewLifecycleOwner, Observer { restaurants ->
            Timber.i("restaurants change triggered")
            if (restaurants != null && restaurants.isNotEmpty()) {
                drawRestaurantsOnMap(restaurants)
            }
        })

        map.onCreate(savedInstanceState)
        map.getMapAsync(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        mContext = context
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        iconGenerator = IconGenerator(requireContext())
    }

    override fun onDetach() {
        super.onDetach()
        mContext = null
    }

    override fun onMapReady(googleMap: GoogleMap) {
        Timber.i("onMapReady")
        googleMap.setOnMarkerClickListener(this)
        mMap = googleMap

        val restaurants = viewModel.restaurants.value
        if (restaurants != null && restaurants.isNotEmpty()) {
            drawRestaurantsOnMap(restaurants)
            drawUserOnMap()
        }
    }

    private fun drawRestaurantsOnMap(restaurants: List<Restaurant>) {
        for (restaurant in restaurants) {
            try {
                val restaurantPosition = LatLng(restaurant.latitude.toDouble(), restaurant.longitude.toDouble())

                val markerOptions = MarkerOptions().icon(BitmapDescriptorFactory.fromBitmap(iconGenerator.makeIcon(restaurant.name)))
                        .position(restaurantPosition)
                        .anchor(iconGenerator.anchorU, iconGenerator.anchorV)

                val marker = mMap?.addMarker(markerOptions)
                marker?.tag = restaurantPosition

            } catch (ex: NumberFormatException) {
                Timber.e(ex, "Lat/Lon are not valid numbers")
            }
        }
    }

    private fun drawUserOnMap() {

        /*if (ActivityCompat.checkSelfPermission(requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(requireContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION),
                    1234)
        }*/

        val task = fusedLocationProviderClient.lastLocation
        task.addOnCompleteListener {
            Timber.tag("LOCATION").d("lat: %s, lon: %s", it.result?.latitude, it.result?.longitude)

            val lat = it.result?.latitude
            val lon = it.result?.longitude

            val userPosition = if(lat != null && lon != null){
                 val position = LatLng(lat, lon)
                customerMarker = mMap?.addMarker(MarkerOptions()
                        .position(position)
                        .title("User"))
                customerMarker?.tag = 0
                position
            } else {
                LatLng(49.19425864740466, 16.605452033127083)
            }

            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(userPosition, 13.5f))
        }
    }

    override fun onResume() {
        super.onResume()
        map.onResume()
    }

    override fun onPause() {
        map.onPause()
        super.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map.onLowMemory()
    }

    override fun onStart() {
        super.onStart()
        map.onStart()
    }

    override fun onStop() {
        super.onStop()
        map.onStop()
    }

    companion object {
        fun newInstance() : MapFragment {
            return MapFragment()
        }
    }

    override fun onMarkerClick(input: Marker?): Boolean {
        input?.let { marker ->
            val tag = marker.tag
            if (tag is LatLng) {
                NavigationUtils.navigateTo(requireActivity(), tag)
            }
        }

        return false
    }
}