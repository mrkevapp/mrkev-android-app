package com.archonalabs.mrkev.interfaces

/**
 * Created by Václav Rychtecký on 07/19/2021
 */
interface SpinnerInterface {
    fun sendBoolean(boolean : Boolean)
    fun openInfoPopUp()
    fun tutorial()
}