package com.archonalabs.mrkev.data.feature.dailymenu

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.feature.dailymenu.DailyMenuLocaleSource
import com.archonalabs.mrkev.domain.feature.dailymenu.DailyMenuRepository
import com.archonalabs.mrkev.domain.feature.dailymenu.DailyMenuSource
import com.archonalabs.mrkev.domain.model.DailyMenu
import timber.log.Timber

/**
 * Created by Jakub Juroska on 2/15/20.
 */
class DailyMenuRepositoryImpl(private val dailyMenuSource: DailyMenuSource, private val dailyMenuLocaleSource: DailyMenuLocaleSource) : DailyMenuRepository {

    override suspend fun fetchDailyMenus(date: String): Result<List<DailyMenu>> {

        //if in db cache and not stale, return data from cache
        if (dailyMenuLocaleSource.hasCachedData()) {
            Timber.i("daily menus loaded from cache")
            return Result.Success(dailyMenuLocaleSource.loadDailyMenu())
        }

        //load data from server
        Timber.i("daily menus will be loaded from server")
        val data = dailyMenuSource.loadDailyMenu(date)

        //save data to db cache
        if (data.isSuccess() && data.getOrNull() != null) {
            Timber.i("daily menus saved to cache")
            dailyMenuLocaleSource.saveDailyMenu(data.getOrNull()!!)
        }

        return data
    }

    override suspend fun fetchOneDailyMenu(date: String, restaurantId: Long): Result<DailyMenu> {

        //if in db cache and not stale, return data from cache
        if (dailyMenuLocaleSource.hasCachedDailyMenu(restaurantId)) {
            Timber.i("daily menu for %d loaded from cache", restaurantId)
            return Result.Success(dailyMenuLocaleSource.loadOneDailyMenu(restaurantId))
        }

        //load data from server
        Timber.i("daily menu for %d will be loaded from server", restaurantId)
        val data = dailyMenuSource.loadOneDailyMenu(date, restaurantId)

        //save data to db cache
        if (data.isSuccess() && data.getOrNull() != null) {
            Timber.i("daily menu for %d saved to cache", restaurantId)
            dailyMenuLocaleSource.saveOneDailyMenu(data.getOrNull()!!)
        }

        return data
    }
}