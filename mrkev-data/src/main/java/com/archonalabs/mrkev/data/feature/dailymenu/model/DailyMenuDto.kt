package com.archonalabs.mrkev.data.feature.dailymenu.model

/**
 * Created by Jakub Juroska on 2/15/20.
 */
data class DailyMenuDto (
    val restaurantID : Int,
    val date : String,
    val soup : List<DailyMenuItemDto>?,
    val mainCourse : List<DailyMenuItemDto>?,
    val dessert : List<DailyMenuItemDto>?,
    val timeFrom : String,
    val timeTo : String
)