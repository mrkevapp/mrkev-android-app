package com.archonalabs.mrkev.data.di

import com.archonalabs.mrkev.data.network.LoggingInterceptor
import com.archonalabs.mrkev.data.network.MrkevApi
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created on 2/9/20.
 */
val networkModule = module {
    factory { LoggingInterceptor() }
    factory { provideOkHttpClient(get()) }
    factory { provideMrkevApi(get()) }
    single { provideRetrofit(get()) }
}

//todo: extract API url into some config file
fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl("https://mrkevapp.com").client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(loggingInterceptor: LoggingInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).build()
}

fun provideMrkevApi(retrofit: Retrofit): MrkevApi = retrofit.create(MrkevApi::class.java)
