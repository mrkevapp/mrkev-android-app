package com.archonalabs.mrkev.data.feature.restaurant.model

/**
 * Created by Jakub Juroska on 2/15/20.
 */
data class RestaurantDto(
    val id: Long,
    val name: String,
    val logoUrl : String,
    val phoneNumber: String,
    val email: String,
    val latitude: String,
    val longitude: String,
    val streetNumber: String,
    val auxNumber: String,
    val street: String,
    val city: String,
    val zip: String
)