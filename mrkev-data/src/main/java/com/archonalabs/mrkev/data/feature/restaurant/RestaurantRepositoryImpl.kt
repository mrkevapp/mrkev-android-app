package com.archonalabs.mrkev.data.feature.restaurant

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.feature.restaurant.RestaurantLocaleSource
import com.archonalabs.mrkev.domain.feature.restaurant.RestaurantRepository
import com.archonalabs.mrkev.domain.feature.restaurant.RestaurantSource
import com.archonalabs.mrkev.domain.model.Restaurant
import timber.log.Timber

/**
 * Created by Jakub Juroska on 2/8/20.
 */
class RestaurantRepositoryImpl(private val restaurantSource: RestaurantSource, private val restaurantLocaleSource: RestaurantLocaleSource) : RestaurantRepository {

    override suspend fun fetchRestaurants(): Result<List<Restaurant>> {

        //if in db cache and not stale, return data from cache
        var data : List<Restaurant> = if (restaurantLocaleSource.hasCachedData()) {
            Timber.tag("RESTAURANT").i("restaurants loaded from cache")
            restaurantLocaleSource.loadRestaurants()
        } else {
            //load data from server
            Timber.tag("RESTAURANT").i("restaurants will be loaded from server")
            val result = restaurantSource.loadRestaurants()

            //save data to db cache
            if (result.isSuccess() && result.getOrNull() != null) {
                Timber.tag("RESTAURANT").i("restaurants saved to cache")
                restaurantLocaleSource.saveRestaurants(result.getOrNull()!!)
                result.getOrNull()!!
            } else {
                listOf<Restaurant>()
            }
        }

        //sort data based on user preference
        if (restaurantLocaleSource.hasFavoriteRestaurant()) {
            //find ID of favorite restaurant
            val restaurantId = restaurantLocaleSource.loadFavoriteRestaurantId()
            //find element in data list with specified ID, can be null
            val restaurant = data.find { it.id == restaurantId }
            restaurant?.let {
                //change list into mutable list
                val mutableData = data.toMutableList()
                //remove favorite restaurant
                mutableData.remove(it)
                //and insert it back at first position
                mutableData.add(0, it)
                //change it back to list
                data = mutableData.toList()
            }
        }

        return Result.Success(data)
    }

    override suspend fun markRestaurantAsFavorite(restaurantId: Long) {
        restaurantLocaleSource.saveFavoriteRestaurantId(restaurantId)
    }

    override suspend fun fetchSingleRestaurant(restaurantId: Long): Result<Restaurant> {

        //if in db cache and not stale, return data from cache
        if (restaurantLocaleSource.hasCachedData(restaurantId)) {
            Timber.tag("RESTAURANT").i("restaurant $restaurantId loaded from cache")
            return Result.Success(restaurantLocaleSource.loadRestaurant(restaurantId)!!)
        }

        //load data from remote/server
        val data = restaurantSource.loadRestaurant(restaurantId)

        //save data to db cache
        if (data.isSuccess() && data.getOrNull() != null) {
            Timber.tag("RESTAURANT").i("restaurant $restaurantId saved to cache")
            restaurantLocaleSource.saveRestaurant(data.getOrNull()!!)
        }

        return data
    }
}