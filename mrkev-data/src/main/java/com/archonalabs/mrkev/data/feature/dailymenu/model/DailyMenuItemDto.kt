package com.archonalabs.mrkev.data.feature.dailymenu.model

/**
 * Created by Jakub Juroska on 2/15/20.
 */
data class DailyMenuItemDto (
        val id : Int,
        val date : String,
        val restaurantID : Int,
        val name : String,
        val price : String?,
        val allergens : String?,
        val tag : String?
)