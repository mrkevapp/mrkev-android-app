package com.archonalabs.mrkev.data.feature.restaurant.mapper

import com.archonalabs.mrkev.data.feature.restaurant.model.RestaurantDao
import com.archonalabs.mrkev.data.feature.restaurant.model.RestaurantDto
import com.archonalabs.mrkev.domain.model.GenericDataModel
import com.archonalabs.mrkev.domain.model.Restaurant

/**
 * Created by Jakub Juroska on 2/15/20.
 */
object RestaurantMapper {
    fun mapToRestaurant(response: GenericDataModel<RestaurantDto>): List<Restaurant> {
        val data = response.data
        return data!!.map {
            mapSingleToRestaurant(it)
        }
    }

    fun mapSingleToRestaurant(response: RestaurantDto): Restaurant {
        return Restaurant(
            response.id,
            response.name,
            response.logoUrl,
            response.phoneNumber,
            response.email,
            response.latitude,
            response.longitude,
            response.streetNumber,
            response.auxNumber,
            response.street,
            response.city,
            response.zip,
            favorite = false
        )
    }

    fun mapToRestaurantDao(data: List<Restaurant>): List<RestaurantDao> {
        return data.map {
            mapSingleToRestaurantDao(it)
        }
    }

    fun mapSingleToRestaurantDao(data: Restaurant): RestaurantDao {
        return RestaurantDao(
                data.id,
                data.name,
                data.logoUrl,
                data.phoneNumber,
                data.email,
                data.latitude,
                data.longitude,
                data.streetNumber,
                data.auxNumber,
                data.street,
                data.city,
                data.zip
        )
    }

    fun mapToRestaurantFromDao(restaurants: List<RestaurantDao>, favoriteRestaurantId : Long): List<Restaurant> {
        return restaurants.map {
            mapSingleToRestaurantFromDao(it, favoriteRestaurantId)
        }
    }

    fun mapSingleToRestaurantFromDao(data: RestaurantDao, favoriteRestaurantId: Long): Restaurant {
        return Restaurant(
                data.id,
                data.name,
                data.logoUrl,
                data.phoneNumber ?: "",
                data.email ?: "",
                data.latitude ?: "",
                data.longitude ?: "",
                data.streetNumber ?: "",
                data.auxNumber ?: "",
                data.street ?: "",
                data.city ?: "",
                data.zip ?: "",
                favorite = data.id == favoriteRestaurantId
        )
    }
}