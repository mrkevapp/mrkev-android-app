package com.archonalabs.mrkev.data.feature.dailymenu.mapper

import com.archonalabs.mrkev.data.feature.dailymenu.mapper.DailyMenuItemMapper.mapToDailyMenuItemDao
import com.archonalabs.mrkev.data.feature.dailymenu.mapper.DailyMenuItemMapper.mapToDailyMenuItemFromDao
import com.archonalabs.mrkev.data.feature.dailymenu.mapper.DailyMenuItemMapper.mapToDailyMenuItemFromDto
import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuDao
import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuDto
import com.archonalabs.mrkev.domain.model.DailyMenu
import com.archonalabs.mrkev.domain.model.GenericDataModel

/**
 * Created by Jakub Juroska on 2/15/20.
 */
const val DEFAULT_FROM_TIME = "08:00"
const val DEFAULT_TO_TIME = "22:00"
object DailyMenuMapper {

    fun mapToDailyMenuFromDto(response: GenericDataModel<DailyMenuDto>): List<DailyMenu> {
        val data = response.data
        return data!!.map {

            val soups = mapToDailyMenuItemFromDto(it.soup ?: emptyList())
            val mainCourses = mapToDailyMenuItemFromDto(it.mainCourse ?: emptyList())
            val desserts = mapToDailyMenuItemFromDto(it.dessert ?: emptyList())

            DailyMenu(
                    it.restaurantID,
                    it.date,
                    soups,
                    mainCourses,
                    desserts,
                    it.timeFrom,
                    it.timeTo
            )
        }
    }

    fun mapToDailyMenuDao(item: DailyMenu): DailyMenuDao {

        val soups = mapToDailyMenuItemDao(item.soup)
        val mainCourses = mapToDailyMenuItemDao(item.mainCourse)
        val desserts = mapToDailyMenuItemDao(item.dessert)

        return DailyMenuDao(
                item.restaurantID,
                item.date,
                soups,
                mainCourses,
                desserts,
                item.timeFrom,
                item.timeTo
        )
    }

    fun mapToDailyMenuDao(data: List<DailyMenu>): List<DailyMenuDao> {
        return data.map {
            mapToDailyMenuDao(it)
        }
    }

    fun mapToDailyMenuFromDao(data : DailyMenuDao) : DailyMenu {
        val soups = mapToDailyMenuItemFromDao(data.soup ?: emptyList())
        val mainCourses = mapToDailyMenuItemFromDao(data.mainCourse ?: emptyList())
        val desserts = mapToDailyMenuItemFromDao(data.dessert ?: emptyList())

        return DailyMenu(
                data.restaurantID,
                data.date,
                soups,
                mainCourses,
                desserts,
                data.timeFrom ?: DEFAULT_FROM_TIME,
                data.timeTo ?: DEFAULT_TO_TIME
        )
    }

    fun mapToDailyMenuFromDao(data: List<DailyMenuDao>): List<DailyMenu> {
        return data.map {
            mapToDailyMenuFromDao(it)
        }
    }
}