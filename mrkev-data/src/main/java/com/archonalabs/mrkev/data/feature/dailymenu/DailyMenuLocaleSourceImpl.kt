package com.archonalabs.mrkev.data.feature.dailymenu

import com.archonalabs.mrkev.data.extension.isMoreThanHourOld
import com.archonalabs.mrkev.data.feature.cache.CacheRecords
import com.archonalabs.mrkev.data.feature.dailymenu.mapper.DailyMenuMapper
import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuDao
import com.archonalabs.mrkev.domain.feature.dailymenu.DailyMenuLocaleSource
import com.archonalabs.mrkev.domain.model.DailyMenu
import io.realm.Realm
import io.realm.RealmResults
import java.util.*

/**
 * Created by Jakub Juroska on 2/15/20.
 */
class DailyMenuLocaleSourceImpl : DailyMenuLocaleSource {

    override suspend fun saveDailyMenu(dailyMenu: List<DailyMenu>) {

        val data = DailyMenuMapper.mapToDailyMenuDao(dailyMenu)

        Realm.getDefaultInstance().use { realm ->

            val oldDailyMenus = getDailyMenus(realm)

            for (oldDailyMenu in oldDailyMenus) {

                if (!data.contains(oldDailyMenu)) {

                    realm.executeTransaction {
                        oldDailyMenu.deleteFromRealm()
                    }
                }
            }

            for (newDailyMenu in data){
                realm.executeTransaction {
                    it.copyToRealmOrUpdate(newDailyMenu)
                }

                updateCacheRecords()
            }
        }
    }

    override suspend fun saveOneDailyMenu(dailyMenu: DailyMenu) {
        val data = DailyMenuMapper.mapToDailyMenuDao(dailyMenu)

        Realm.getDefaultInstance().use { realm ->

            realm.executeTransaction {
                it.copyToRealmOrUpdate(data)
            }
        }
    }

    private fun updateCacheRecords() {
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            val cacheRecords = realm.where(CacheRecords::class.java).findFirst()
            realm.executeTransaction {
                cacheRecords?.dailyMenuRefreshTimestamp = Date()
            }
        }
    }

    override suspend fun loadDailyMenu(): List<DailyMenu> {
        return Realm.getDefaultInstance().use { realm ->
            DailyMenuMapper.mapToDailyMenuFromDao(getDailyMenus(realm))
        }
    }

    //using !! because we check before calling, that we have daily menu for specified ID in DB
    override suspend fun loadOneDailyMenu(restaurantId : Long): DailyMenu {
        return Realm.getDefaultInstance().use { realm ->
            DailyMenuMapper.mapToDailyMenuFromDao(getOneDailyMenu(realm, restaurantId)!!)
        }
    }

    override fun hasCachedData(): Boolean {
        Realm.getDefaultInstance().use { realm ->
            if (isDailyMenuCacheStale(realm)) {
                return false
            }

            if (getDailyMenus(realm).isNotEmpty()) {
                return true
            }

            return false
        }
    }

    override fun hasCachedDailyMenu(restaurantId: Long): Boolean {
        return Realm.getDefaultInstance().use { it.where(DailyMenuDao::class.java).equalTo("restaurantID", restaurantId).findFirst() != null }
    }

    private fun getDailyMenus(realm: Realm) : RealmResults<DailyMenuDao> {
        return realm.where(DailyMenuDao::class.java).findAll()
    }

    private fun getOneDailyMenu(realm: Realm, restaurantId: Long) : DailyMenuDao? {
        return realm.where(DailyMenuDao::class.java).equalTo("restaurantID", restaurantId).findFirst()
    }

    private fun isDailyMenuCacheStale(realm: Realm) : Boolean {
        //cache records is unique / singleton
        val cacheRecords = realm.where(CacheRecords::class.java).findFirst()
        return cacheRecords == null || cacheRecords.dailyMenuRefreshTimestamp.isMoreThanHourOld(1)
    }
}