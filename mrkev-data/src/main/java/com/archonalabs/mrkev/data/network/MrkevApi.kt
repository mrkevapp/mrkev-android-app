package com.archonalabs.mrkev.data.network

import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuDto
import com.archonalabs.mrkev.data.feature.restaurant.model.RestaurantDto
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created on 2/9/20.
 */
interface MrkevApi {

    @GET("restaurant/{restaurantId}")
    fun getRestaurant(
            @Path("restaurantId") restaurantId : Long
    ): Call<RestaurantDto>

    @GET("restaurants")
    fun getRestaurants(
    ): Call<com.archonalabs.mrkev.domain.model.GenericDataModel<RestaurantDto>>

    @GET("dailymenus/{date}")
    fun getDailyMenu(
            @Path("date") date : String
    ): Call<com.archonalabs.mrkev.domain.model.GenericDataModel<DailyMenuDto>>

    @GET("dailymenu/{restaurantId}/{date}")
    fun getDailyMenu(
            @Path("restaurantId") restaurantId : Long,
            @Path("date") date : String
    ): Call<com.archonalabs.mrkev.domain.model.GenericDataModel<DailyMenuDto>>

}