package com.archonalabs.mrkev.data.feature.restaurant

import com.archonalabs.mrkev.data.extension.isMoreThanHourOld
import com.archonalabs.mrkev.data.feature.cache.CacheRecords
import com.archonalabs.mrkev.data.feature.restaurant.mapper.RestaurantMapper
import com.archonalabs.mrkev.data.feature.restaurant.model.FAVORITE_NO_VALUE
import com.archonalabs.mrkev.data.feature.restaurant.model.FavoriteRestaurantDao
import com.archonalabs.mrkev.data.feature.restaurant.model.RestaurantDao
import com.archonalabs.mrkev.domain.feature.restaurant.RestaurantLocaleSource
import com.archonalabs.mrkev.domain.model.Restaurant
import io.realm.Realm
import io.realm.RealmResults
import timber.log.Timber
import java.util.*

/**
 * Created by Jakub Juroska on 2/15/20.
 */
class RestaurantLocaleSourceImpl : RestaurantLocaleSource {

    override suspend fun saveRestaurants(restaurants: List<Restaurant>) {

        val data = RestaurantMapper.mapToRestaurantDao(restaurants)

        Realm.getDefaultInstance().use { realm ->

            val oldRestaurants = getRestaurants(realm)
            for (oldRestaurant in oldRestaurants) {

                if (!data.contains(oldRestaurant)) {
                    realm.executeTransaction {
                        oldRestaurant.deleteFromRealm()
                    }
                }
            }

            for (newRestaurant in data){
                realm.executeTransaction {
                    realm.copyToRealmOrUpdate(newRestaurant)
                }

                updateCacheRecords()
            }
        }
    }

    override suspend fun saveRestaurant(restaurant: Restaurant) {
        val data = RestaurantMapper.mapSingleToRestaurantDao(restaurant)
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction {
                realm.copyToRealmOrUpdate(data)
            }
        }
    }

    private fun updateCacheRecords() {
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            val cacheRecords = realm.where(CacheRecords::class.java).findFirst()
            cacheRecords?.let { records ->
                realm.executeTransaction {
                    records.restaurantRefreshTimestamp = Date()
                    it.copyToRealmOrUpdate(records)
                }
            } ?: Timber.w("CacheRecords should never be null")
        }
    }

    override suspend fun loadRestaurants(): List<Restaurant> {
        val realmAccess = Realm.getDefaultInstance()
        return realmAccess.use { realm ->

            RestaurantMapper.mapToRestaurantFromDao(getRestaurants(realm), loadFavoriteRestaurantId())
        }
    }

    override suspend fun loadRestaurant(restaurantId: Long): Restaurant? {
        val realmAccess = Realm.getDefaultInstance()
        return realmAccess.use { realm ->
            val restaurant = getRestaurant(realm, restaurantId)
            restaurant?.let {
                RestaurantMapper.mapSingleToRestaurantFromDao(it, loadFavoriteRestaurantId())
            }
        }
    }

    override suspend fun hasCachedData(): Boolean {
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            if (isRestaurantCacheStale(realm)) {
                Timber.tag("RESTAURANT").d("restaurant cache is stalled")
                return false
            }

            if (getRestaurants(realm).isNotEmpty()) {
                Timber.tag("RESTAURANT").d("cache is not empty without long")
                return true
            }

            return false
        }
    }

    override suspend fun hasCachedData(restaurantId: Long): Boolean {
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            if (isRestaurantCacheStale(realm)) {
                Timber.tag("RESTAURANT").d("restaurant cache is stalled")
                return false
            }

            if (getRestaurant(realm, restaurantId) != null) {
                Timber.tag("RESTAURANT").d("cache is not empty")
                return true
            }

            return false
        }
    }

    override suspend fun hasFavoriteRestaurant(): Boolean {
        val realmAccess = Realm.getDefaultInstance()
        realmAccess.use { realm ->
            val favorite = realm.where(FavoriteRestaurantDao::class.java).findFirst()
            if (favorite != null && favorite.restaurantID != FAVORITE_NO_VALUE) {
                return true
            }

            return false
        }
    }

    override suspend fun loadFavoriteRestaurantId(): Long {
        val realmAccess = Realm.getDefaultInstance()
        return realmAccess.use { realm ->
            val favorite = realm.where(FavoriteRestaurantDao::class.java).findFirst()
            favorite?.restaurantID ?: FAVORITE_NO_VALUE
        }
    }

    override suspend fun saveFavoriteRestaurantId(restaurantId : Long) {
        val realmAccess = Realm.getDefaultInstance()
        return realmAccess.use { realm ->
            val favorite = realm.where(FavoriteRestaurantDao::class.java).findFirst()
            favorite?.let { favoriteRestaurantDao ->
                realm.executeTransaction {
                    favoriteRestaurantDao.restaurantID = restaurantId
                }
            } ?: run {
                realm.executeTransaction {
                    val newFavorite = realm.createObject(FavoriteRestaurantDao::class.java)
                    newFavorite.restaurantID = restaurantId
                }
            }
        }
    }

    private fun getRestaurants(realm: Realm): RealmResults<RestaurantDao> {
        return realm.where(RestaurantDao::class.java).findAll()
    }

    private fun getRestaurant(realm: Realm, restaurantId: Long): RestaurantDao? {
        return realm.where(RestaurantDao::class.java)
                .equalTo("id", restaurantId)
                .findFirst()
    }

    private fun isRestaurantCacheStale(realm: Realm): Boolean {
        //cache records is unique / singleton
        val cacheRecords = realm.where(CacheRecords::class.java).findFirst()
        return cacheRecords == null || cacheRecords.restaurantRefreshTimestamp.isMoreThanHourOld(1)
    }
}