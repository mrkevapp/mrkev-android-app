package com.archonalabs.mrkev.data.feature.dailymenu.mapper

import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuItemDao
import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuItemDto
import com.archonalabs.mrkev.domain.model.DailyMenuItem
import io.realm.RealmList

/**
 * Created by Jakub Juroska on 2/15/20.
 */
object DailyMenuItemMapper {

    fun mapToDailyMenuItemFromDto(data: List<DailyMenuItemDto>): List<DailyMenuItem> {
        return data.map {
            DailyMenuItem(
                it.id,
                it.date,
                it.restaurantID,
                it.name,
                it.price ?: "",
                it.allergens ?: "",
                it.tag ?: ""
            )
        }
    }

    fun mapToDailyMenuItemDao(data: List<DailyMenuItem>): RealmList<DailyMenuItemDao> {

        val realmList = RealmList<DailyMenuItemDao>()
        realmList.addAll(data.map {
            DailyMenuItemDao(
                    it.id,
                    it.date,
                    it.restaurantID,
                    it.name,
                    it.price,
                    it.allergens,
                    it.tag
            )
        })

        return realmList
    }

    fun mapToDailyMenuItemFromDao(data: List<DailyMenuItemDao>): List<DailyMenuItem> {

        return data.map {
            DailyMenuItem(
                    it.id,
                    it.date,
                    it.restaurantID,
                    it.name ?: "",
                    it.price ?: "",
                    it.allergens ?: "",
                    it.tag ?: ""
            )
        }
    }
}