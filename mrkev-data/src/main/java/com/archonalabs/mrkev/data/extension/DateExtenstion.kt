package com.archonalabs.mrkev.data.extension

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Jakub Juroska on 2/16/20.
 */
fun Date.isMoreThanHourOld(hours : Int): Boolean {
    val cal = Calendar.getInstance()
    cal.add(Calendar.HOUR, -hours)
    return (this.before(cal.time))
}

fun Date.getCurrentDateString(): String {
    val today = Calendar.getInstance().time
    val format = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
    return format.format(today)
}