package com.archonalabs.mrkev.data.feature.restaurant.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by Jakub Juroska on 2/15/20.
 */
@RealmClass
open class RestaurantDao(

        @PrimaryKey
        var id: Long = 0,
        var name: String = "",
        var logoUrl : String = "",
        var phoneNumber: String? = null,
        var email: String? = null,
        var latitude: String? = null,
        var longitude: String? = null,
        var streetNumber: String? = null,
        var auxNumber: String? = null,
        var street: String? = null,
        var city: String? = null,
        var zip: String? = null

) : RealmObject()