package com.archonalabs.mrkev.data.feature.dailymenu

import com.archonalabs.mrkev.data.feature.dailymenu.mapper.DailyMenuMapper
import com.archonalabs.mrkev.data.network.MrkevApi
import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.exception.EmptyBodyException
import com.archonalabs.mrkev.domain.feature.dailymenu.DailyMenuSource
import com.archonalabs.mrkev.domain.model.ApiErrorResult
import com.archonalabs.mrkev.domain.model.DailyMenu
import com.archonalabs.mrkev.domain.safeCall
import retrofit2.awaitResponse

/**
 * Created by Jakub Juroska on 2/15/20.
 */
class DailyMenuSourceImpl(val mrkevApi: MrkevApi) : DailyMenuSource {

    override suspend fun loadDailyMenu(date: String): Result<List<DailyMenu>> {
        return safeCall(
                call = { fetchDailyMenu(date) },
                errorMessage = "Cannot load Daily menu - Unexpected error"
        )
    }

    private suspend fun fetchDailyMenu(date : String): Result<List<DailyMenu>> {

        val response = mrkevApi.getDailyMenu(date).awaitResponse()
        return if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                Result.Success(data = DailyMenuMapper.mapToDailyMenuFromDto(body))
            } else {
                Result.Error(
                    ApiErrorResult(
                        code = response.code(),
                        errorMessage = response.message(),
                        apiThrowable = EmptyBodyException()
                    )
                )
            }
        } else {
            Result.Error(ApiErrorResult(code = response.code(), errorMessage = response.message()))
        }

    }

    override suspend fun loadOneDailyMenu(date: String, restaurantId: Long): Result<DailyMenu> {
        return safeCall(
                call = { fetchOneDailyMenu(date, restaurantId) },
                errorMessage = "Cannot load single Daily menu - Unexpected error"
        )
    }

    private suspend fun fetchOneDailyMenu(date: String, restaurantId: Long): Result<DailyMenu> {
        val response = mrkevApi.getDailyMenu(restaurantId, date).awaitResponse()
        return if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                Result.Success(data = DailyMenuMapper.mapToDailyMenuFromDto(body).first())
            } else {
                Result.Error(
                    ApiErrorResult(
                        code = response.code(),
                        errorMessage = response.message(),
                        apiThrowable = EmptyBodyException()
                    )
                )
            }
        } else {
            Result.Error(ApiErrorResult(code = response.code(), errorMessage = response.message()))
        }
    }
}