package com.archonalabs.mrkev.data.di

import com.archonalabs.mrkev.data.feature.dailymenu.DailyMenuLocaleSourceImpl
import com.archonalabs.mrkev.data.feature.dailymenu.DailyMenuRepositoryImpl
import com.archonalabs.mrkev.data.feature.dailymenu.DailyMenuSourceImpl
import com.archonalabs.mrkev.data.feature.restaurant.RestaurantLocaleSourceImpl
import com.archonalabs.mrkev.data.feature.restaurant.RestaurantRepositoryImpl
import com.archonalabs.mrkev.data.feature.restaurant.RestaurantSourceImpl
import com.archonalabs.mrkev.data.realm.MrkevRealmMigration
import com.archonalabs.mrkev.data.realm.MrkevRealmModule
import com.archonalabs.mrkev.domain.feature.dailymenu.*
import com.archonalabs.mrkev.domain.feature.restaurant.*
import org.koin.dsl.module
/**
 * Created by Jakub Juroska on 2/8/20.
 */
val mrkevModule = module {

    single { FetchRestaurantsUseCase(get()) }

    single { FetchSingleRestaurantUseCase(get()) }

    single<RestaurantSource> { RestaurantSourceImpl(get()) }

    single<RestaurantLocaleSource> { RestaurantLocaleSourceImpl() }

    single<RestaurantRepository> { RestaurantRepositoryImpl(get(), get()) }

    single { FetchDailyMenuUseCase(get()) }

    single { FetchOneDailyMenuUseCase(get()) }

    single { MarkRestaurantAsFavoriteUseCase(get()) }

    single<DailyMenuSource> { DailyMenuSourceImpl(get()) }

    single<DailyMenuLocaleSource> { DailyMenuLocaleSourceImpl() }

    single<DailyMenuRepository> { DailyMenuRepositoryImpl(get(), get()) }

    single { MrkevRealmModule() }

    single { MrkevRealmMigration() }
}

