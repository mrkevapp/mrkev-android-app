package com.archonalabs.mrkev.data.feature.restaurant

import com.archonalabs.mrkev.data.feature.restaurant.mapper.RestaurantMapper
import com.archonalabs.mrkev.data.network.MrkevApi
import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.exception.EmptyBodyException
import com.archonalabs.mrkev.domain.feature.restaurant.RestaurantSource
import com.archonalabs.mrkev.domain.model.ApiErrorResult
import com.archonalabs.mrkev.domain.model.Restaurant
import com.archonalabs.mrkev.domain.safeCall
import retrofit2.awaitResponse

/**
 * Created by Jakub Juroska on 2/15/20.
 */
class RestaurantSourceImpl(private val mrkevApi: MrkevApi) : RestaurantSource {

    override suspend fun loadRestaurants(): Result<List<Restaurant>> {
       return safeCall(
              call = { fetchRestaurants() },
              errorMessage = "Cannot load Restaurants - Unexpected error"
       )
    }

    private suspend fun fetchRestaurants() : Result<List<Restaurant>> {
        val response = mrkevApi.getRestaurants().awaitResponse()
        return if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                Result.Success(data = RestaurantMapper.mapToRestaurant(body))
            } else {
                Result.Error(
                        ApiErrorResult(
                                code = response.code(),
                                errorMessage = response.message(),
                                apiThrowable = EmptyBodyException()
                        )
                )
            }
        } else {
            Result.Error(ApiErrorResult(code = response.code(), errorMessage = response.message()))
        }
    }

    override suspend fun loadRestaurant(restaurantId: Long): Result<Restaurant> {
        return safeCall(
                call = { fetchRestaurant(restaurantId = restaurantId) },
                errorMessage = "Cannot load Restaurant - Unexpected error"
        )
    }

    private suspend fun fetchRestaurant(restaurantId : Long): Result<Restaurant> {
        val response = mrkevApi.getRestaurant(restaurantId).awaitResponse()
        return if (response.isSuccessful) {
            val body = response.body()
            if (body != null) {
                Result.Success(data = RestaurantMapper.mapSingleToRestaurant(body))
            } else {
                Result.Error(
                        ApiErrorResult(
                                code = response.code(),
                                errorMessage = response.message(),
                                apiThrowable = EmptyBodyException()
                        )
                )
            }
        } else {
            Result.Error(ApiErrorResult(code = response.code(), errorMessage = response.message()))
        }
    }
}