package com.archonalabs.mrkev.data.feature.dailymenu.model

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by Jakub Juroska on 2/15/20.
 */
@RealmClass
open class DailyMenuDao(

        @PrimaryKey
        var restaurantID : Int = 0,
        var date : String = "",
        var soup : RealmList<DailyMenuItemDao>? = null,
        var mainCourse : RealmList<DailyMenuItemDao>? = null,
        var dessert : RealmList<DailyMenuItemDao>? = null,
        var timeFrom : String? = null,
        var timeTo : String? = null

) : RealmObject()