package com.archonalabs.mrkev.data.feature.cache

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

/**
 * Created by Jakub Juroska on 2/16/20.
 */
@RealmClass
open class CacheRecords(

        @PrimaryKey
        var id : Int = 0,
        var restaurantRefreshTimestamp : Date = Date(0),
        var dailyMenuRefreshTimestamp : Date = Date(0)

) : RealmObject()