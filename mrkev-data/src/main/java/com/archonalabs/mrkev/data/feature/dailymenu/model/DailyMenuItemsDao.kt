package com.archonalabs.mrkev.data.feature.dailymenu.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by Jakub Juroska on 2/15/20.
 */
@RealmClass
open class DailyMenuItemDao(

        @PrimaryKey
        var id : Int = 0,
        var date : String = "",
        var restaurantID : Int = 0,
        var name : String? = null,
        var price : String? = null,
        var allergens : String? = null,
        var tag : String? = null

) : RealmObject()