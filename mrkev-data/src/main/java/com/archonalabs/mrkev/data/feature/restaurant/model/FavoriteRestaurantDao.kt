package com.archonalabs.mrkev.data.feature.restaurant.model

import io.realm.RealmObject
import io.realm.annotations.RealmClass

/**
 * Created by Jakub Juroska on 3/2/20.
 */
const val FAVORITE_NO_VALUE = -1L

@RealmClass
open class FavoriteRestaurantDao(
        var restaurantID : Long = FAVORITE_NO_VALUE
) : RealmObject()