package com.archonalabs.mrkev.data.realm

import com.archonalabs.mrkev.data.feature.cache.CacheRecords
import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuDao
import com.archonalabs.mrkev.data.feature.dailymenu.model.DailyMenuItemDao
import com.archonalabs.mrkev.data.feature.restaurant.model.FavoriteRestaurantDao
import com.archonalabs.mrkev.data.feature.restaurant.model.RestaurantDao
import io.realm.annotations.RealmModule

/**
 * Created by Jakub Juroska on 2/16/20.
 */
@RealmModule(classes = [RestaurantDao::class, DailyMenuDao::class, DailyMenuItemDao::class, CacheRecords::class, FavoriteRestaurantDao::class])
open class MrkevRealmModule {
}