package com.archonalabs.mrkev.data.realm

import io.realm.DynamicRealm
import io.realm.RealmMigration


/**
 * Created by Jakub Juroska on 3/3/20.
 */
class MrkevRealmMigration : RealmMigration {

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        var oldVersion = oldVersion
        val schema = realm.schema

        //Prepared for migration, current version is 1, so no migration will be triggered.
        //when we change version to 2, it will trigger next lines of code
        if (oldVersion == 1L) {
            val profileUserSchema = schema["FavoriteRestaurantDao"]
            //migrate data here?
            // Delete all other data than `FavoriteRestaurantDao`
            for (classSchema in schema.all) {
                if (classSchema.className == "FavoriteRestaurantDao") {
                    continue
                }
                realm.delete(classSchema.className)
            }
            oldVersion++
        }
    }
}