package com.archonalabs.mrkev.domain.feature.restaurant

import com.archonalabs.mrkev.domain.model.Restaurant

/**
 * Created by Jakub Juroska on 2/15/20.
 */
interface RestaurantLocaleSource {
    suspend fun saveRestaurants(restaurants : List<Restaurant>)

    suspend fun saveRestaurant(restaurant : Restaurant)

    suspend fun loadRestaurants() : List<Restaurant>

    suspend fun loadRestaurant(restaurantId: Long): Restaurant?

    suspend fun hasCachedData(): Boolean

    suspend fun hasCachedData(restaurantId: Long): Boolean

    suspend fun hasFavoriteRestaurant(): Boolean

    suspend fun loadFavoriteRestaurantId(): Long

    suspend fun saveFavoriteRestaurantId(restaurantId: Long)


}