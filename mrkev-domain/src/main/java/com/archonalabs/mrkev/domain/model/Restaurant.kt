package com.archonalabs.mrkev.domain.model

/**
 * Created by Jakub Juroska on 2/8/20.
 */
data class Restaurant(
    val id: Long,
    val name: String,
    val logoUrl : String,
    val phoneNumber: String,
    val email: String,
    val latitude: String,
    val longitude: String,
    val streetNumber: String,
    val auxNumber: String,
    val street: String,
    val city: String,
    val zip: String,

    val favorite : Boolean
)