package com.archonalabs.mrkev.domain.model

import com.archonalabs.mrkev.domain.ErrorResult

/**
 * Created by Jakub Juroska on 2/15/20.
 */
data class ApiErrorResult(val code: Int,
                          val errorMessage: String? = null,
                          val apiThrowable: Throwable? = null) : ErrorResult(errorMessage, apiThrowable)