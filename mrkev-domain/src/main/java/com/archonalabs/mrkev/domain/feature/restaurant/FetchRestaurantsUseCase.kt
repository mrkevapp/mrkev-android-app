package com.archonalabs.mrkev.domain.feature.restaurant

import com.archonalabs.mrkev.domain.model.Restaurant
import com.archonalabs.mrkev.domain.usecases.UseCaseResultNoParams

/**
 * Created by Jakub Juroska on 2/8/20.
 */

class FetchRestaurantsUseCase(private val restaurantRepository: RestaurantRepository) : UseCaseResultNoParams<List<Restaurant>>() {

    override suspend fun doWork(params: Unit) = restaurantRepository.fetchRestaurants()

}