package com.archonalabs.mrkev.domain.exception

import java.io.IOException

/**
 * Created by Jakub Juroska on 2/15/20.
 */
class EmptyBodyException : IOException("Response with empty body")