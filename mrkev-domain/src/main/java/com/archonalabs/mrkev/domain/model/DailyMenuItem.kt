package com.archonalabs.mrkev.domain.model

/**
 * Created by Jakub Juroska on 2/8/20.
 */
data class DailyMenuItem(
    val id : Int,
    val date : String,
    val restaurantID : Int,
    val name : String,
    val price : String,
    val allergens : String,
    val tag : String
)