package com.archonalabs.mrkev.domain.feature.restaurant

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.model.Restaurant
import com.archonalabs.mrkev.domain.usecases.UseCaseResult

/**
 * Created by Jakub Juroska on 3/7/20.
 */
class FetchSingleRestaurantUseCase(private val restaurantRepository: RestaurantRepository) : UseCaseResult<Restaurant, FetchSingleRestaurantUseCase.Params>() {

    override suspend fun doWork(params: Params): Result<Restaurant> = restaurantRepository.fetchSingleRestaurant(params.restaurantId)

    data class Params(val restaurantId: Long)
}