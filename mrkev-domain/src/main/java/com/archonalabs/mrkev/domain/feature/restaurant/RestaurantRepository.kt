package com.archonalabs.mrkev.domain.feature.restaurant

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.model.Restaurant

/**
 * Created by Jakub Juroska on 2/8/20.
 */
interface RestaurantRepository {
    suspend fun fetchRestaurants() : Result<List<Restaurant>>

    suspend fun markRestaurantAsFavorite(restaurantId: Long)

    suspend fun fetchSingleRestaurant(restaurantId: Long): Result<Restaurant>
}