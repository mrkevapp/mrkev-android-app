package com.archonalabs.mrkev.domain.feature.dailymenu

import com.archonalabs.mrkev.domain.model.DailyMenu

/**
 * Created by Jakub Juroska on 2/15/20.
 */
interface DailyMenuLocaleSource {
    suspend fun saveDailyMenu(dailyMenu : List<DailyMenu>)

    suspend fun saveOneDailyMenu(dailyMenu: DailyMenu)

    suspend fun loadDailyMenu() : List<DailyMenu>

    suspend fun loadOneDailyMenu(restaurantId : Long) : DailyMenu

    fun hasCachedData() : Boolean

    fun hasCachedDailyMenu(restaurantId: Long): Boolean
}