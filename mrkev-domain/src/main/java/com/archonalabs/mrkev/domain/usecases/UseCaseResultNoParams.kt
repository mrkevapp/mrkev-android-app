package com.archonalabs.mrkev.domain.usecases

import com.archonalabs.mrkev.domain.Result

/**
 * Created by Jakub Juroska on 2/15/20.
 */
abstract class UseCaseResultNoParams<out T : Any> : UseCase<Result<T>, Unit>() {

    suspend operator fun invoke() = super.invoke(Unit)

}