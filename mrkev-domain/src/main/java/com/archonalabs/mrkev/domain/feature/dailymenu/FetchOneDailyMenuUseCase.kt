package com.archonalabs.mrkev.domain.feature.dailymenu

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.model.DailyMenu
import com.archonalabs.mrkev.domain.usecases.UseCaseResult

/**
 * Created by Jakub Juroska on 2/16/20.
 */
class FetchOneDailyMenuUseCase(private val dailyMenuRepository: DailyMenuRepository) : UseCaseResult<DailyMenu, FetchOneDailyMenuUseCase.Params>() {

    override suspend fun doWork(params: Params): Result<DailyMenu> {
        return dailyMenuRepository.fetchOneDailyMenu(params.date, params.restaurantId)
    }

    data class Params(val date: String, val restaurantId : Long)
}