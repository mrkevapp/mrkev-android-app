package com.archonalabs.mrkev.domain.feature.dailymenu

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.model.DailyMenu

/**
 * Created by Jakub Juroska on 2/15/20.
 */
interface DailyMenuRepository {
    suspend fun fetchDailyMenus(date : String) : Result<List<DailyMenu>>

    suspend fun fetchOneDailyMenu(date : String, restaurantId : Long) : Result<DailyMenu>
}