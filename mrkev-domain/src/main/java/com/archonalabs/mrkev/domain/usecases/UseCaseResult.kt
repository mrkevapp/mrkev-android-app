package com.archonalabs.mrkev.domain.usecases

import com.archonalabs.mrkev.domain.Result

/**
 * Created by Jakub Juroska on 2/15/20.
 */
abstract class UseCaseResult<out T : Any, in Params> : UseCase<Result<T>, Params>()