package com.archonalabs.mrkev.domain.model

/**
 * Created by Jakub Juroska on 6/4/19.
 */
class GenericDataModel<T> {
    var data : List<T>? = null
}