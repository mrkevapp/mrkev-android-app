package com.archonalabs.mrkev.domain.feature.restaurant

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.model.Restaurant

/**
 * Created by Jakub Juroska on 2/15/20.
 */
interface RestaurantSource {
    suspend fun loadRestaurants(): Result<List<Restaurant>>

    suspend fun loadRestaurant(restaurantId : Long): Result<Restaurant>
}