package com.archonalabs.mrkev.domain.model

/**
 * Created by Jakub Juroska on 2/8/20.
 */
data class DailyMenu (
    val restaurantID : Int,
    val date : String,
    val soup : List<DailyMenuItem>,
    val mainCourse : List<DailyMenuItem>,
    val dessert : List<DailyMenuItem>,
    val timeFrom : String,
    val timeTo : String
)