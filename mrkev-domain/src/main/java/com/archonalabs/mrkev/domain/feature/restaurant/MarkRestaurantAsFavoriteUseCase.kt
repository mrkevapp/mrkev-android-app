package com.archonalabs.mrkev.domain.feature.restaurant

import com.archonalabs.mrkev.domain.usecases.UseCase

/**
 * Created by Jakub Juroska on 3/3/20.
 */
class MarkRestaurantAsFavoriteUseCase(private val restaurantRepository: RestaurantRepository) : UseCase<Unit, MarkRestaurantAsFavoriteUseCase.Params>() {

    override suspend fun doWork(params: Params) = restaurantRepository.markRestaurantAsFavorite(params.restaurantId)

    data class Params(val restaurantId: Long)
}