package com.archonalabs.mrkev.domain.feature.dailymenu

import com.archonalabs.mrkev.domain.Result
import com.archonalabs.mrkev.domain.model.DailyMenu
import com.archonalabs.mrkev.domain.usecases.UseCaseResult

/**
 * Created by Jakub Juroska on 2/15/20.
 */
class FetchDailyMenuUseCase(private val dailyMenuRepository: DailyMenuRepository) : UseCaseResult<List<DailyMenu>, FetchDailyMenuUseCase.Params>() {

    override suspend fun doWork(params: Params): Result<List<DailyMenu>> {
        return dailyMenuRepository.fetchDailyMenus(params.date)
    }

    data class Params(val date: String)
}